FORMAT: 1A

# Codekamp API

# Authentication
User resource representation.

## Login User [GET /authenticate]
Returns full user object with login token used to acess user account.

+ Parameters
    + email: (string, required) - Email id of the user.
    + password: (string, required) - Password of the user.

# User
User Resource representation

## Show the user [GET /me]
Get a JSON representation of all the details of currently logged in user.

# Batches [/batches]
Batch resource representation

## Show all batches. [GET /batches]
Get a JSON representation of all the batches.

+ Parameters
    + include: (string, optional) - Can have two values 'contacts' and 'technology'

## Store a new Batch [POST /batches]
Store a new batch with the specified name.

+ Request (application/x-www-form-urlencoded)
    + Body

                 *     name: (string, required) - Name Of the Batch.
                 *     technology_id: (integer, required) - Id of technology

## Show a Batch. [GET /batches/{id}]
Show details of a batch specified by given id.

+ Parameters
    + include: (string, optional) - Can have two values 'contacts' and 'technology'

## Show Contacts in a Batch. [GET /batches/{id}/contacts]
Shows a list of contacts associated by the specified Batch id.

+ Parameters
    + include: (string, optional) - Can be - 'batches' , 'interestedTechnologies' , 'acquaintedTechnologies' , 'comments'

## Store a Contact in a Batch. [POST /batches/{id}/contact]


+ Request (application/x-www-form-urlencoded)
    + Body

                 *      contact_id: (array, required) - Array of Contact Ids

# Lists [/lists]
List Resource representation

## Show all lists. [GET /lists]
Get a JSON representation of all the Lists of Contacts.

## Create a new List [POST /lists]


+ Request (application/x-www-form-urlencoded)
    + Body

                 *     title: (string, required) - Title of list
                 *     description: (string, required) - Description of list.

## Show details of List. [GET /lists/{id}]
Get a JSON representation of all the details of any List.

## Show Contacts in a List. [GET /lists/{id}/contacts]
Shows a list of contacts associated with any List.

+ Parameters
    + include: (string, optional) - Can be - 'batches' , 'interestedTechnologies' , 'acquaintedTechnologies' , 'comments'

## Store Contact in List. [POST /lists/{id}/contact]


+ Request (application/x-www-form-urlencoded)
    + Body

                 *      contact_id: (array, required) - Array Of Contact Ids.

# Contacts [/contacts]
Contact Resource representation

## Search contact by name. [GET /contacts/fetch]
Get a JSON representation of all the contacts whose names are similar to 'name' parameter.

+ Parameters
    + name: (string, required) - String to be searched in names

## Show all contacts. [GET /contacts]
Get a paginated JSON representation of all the Contacts.

+ Parameters
    + include: (string, optional) - Can be - 'batches' , 'interestedTechnologies' , 'acquaintedTechnologies' , 'comments'

## Store a new Contact [POST /contacts]


+ Request (application/x-www-form-urlencoded)
    + Body

                 *     first_name: (string, required) - First name of the contact.
                 *     last_name: (string, optional) - Last name of the contact.
                 *     batch_id: (integer, optional) - Batch Id in which contact is to be added..
                 *     list_id: (integer, optional) - List Id in which contact is to be added.
                 *     phones: (array, optional) - Phone Numbers of the contact.
                 *     emails: (array, optional) - Email Ids of the contact.

## Show details of contact. [GET /contacts/{id}]
Get a JSON representation of all the details of any Contact.

+ Parameters
    + include: (string, optional) - Can be - 'batches' , 'interestedTechnologies' , 'acquaintedTechnologies' , 'comments'

## List the comments. [GET /contacts/{id}/comments]
.

Get a JSON representation of all the comments made by any Contact.

## Store new comment. [POST /contacts/{id}/comments]
Associate a new comment with the contact

+ Request (application/x-www-form-urlencoded)
    + Body

                 *     comment: (string, required) - Comment made by the contact.

# Technologies [/technologies]
Technology Resource representation

## Show all technologies [GET /technologies]
Get a JSON representation of all the technologies.

+ Parameters
    + include: (string, optional) - Can be - 'batches' , 'acquaintedContacts' , 'interestedContacts'

## Store a new technology [POST /technologies]


+ Request (application/x-www-form-urlencoded)
    + Body

                 *     title: (string, required) - Name of Technology.

## Show details of Technology. [GET /technologies/{id}]


+ Parameters
    + include: (string, optional) - Can be - 'batches' , 'acquaintedContacts' , 'interestedContacts'

## Show all Batches of Technology. [GET /technologies/{id}/batches]


+ Parameters
    + include: (string, optional) - Can have two values 'contacts' and 'technologies'